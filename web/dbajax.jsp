<%-- 
    Document   : dbajax
    Created on : 29 Apr, 2020, 10:34:17 PM
    Author     : Dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script src="jquery-3.5.0.min.js"></script>
        <script>
            function loaddata()
            {
                var DataString="option=display";
                $.ajax({
                    url:"dbAjax", data:DataString, type:"post",
                    success: function(data)
                    {
                        $("#dbdata").html(data);
                    }
                });
            }
        </script>
    </head>
    <body>
        <h1>Hello World!</h1>
        <input type="button" onclick="loaddata()">
        <div id="dbdata"></div>
    </body>
</html>
