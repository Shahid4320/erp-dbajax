/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CAMPS.dbajax;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import CAMPS.Connect.DBConnect;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class dbAjax extends HttpServlet {


    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        DBConnect db=new DBConnect();
        String data="";
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            if (request.getParameter("option")==null){
            out.print("no option is specified");
            }else if(request.getParameter("option").equalsIgnoreCase("display")) {
            db.getConnection();
            db.read("select id, student_name,student_dob from test.student_det");
            data+="<table>";
            while (db.rs.next())
            {
                data+="<tr>";
                data+="<td>"+db.rs.getString("id")+"</td>"+"<td>"+db.rs.getString("student_name")+"</td>"+"<td>"+db.rs.getString("student_dob")+"</td>";
                data+="</tr>";
            }
            data+="</table>";
            out.print(data);
            db.closeConnection();
            }
            else
                out.print("option undefined");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(dbAjax.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(dbAjax.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(dbAjax.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(dbAjax.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }


    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
